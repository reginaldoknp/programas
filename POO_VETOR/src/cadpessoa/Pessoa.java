package cadpessoa;


public class Pessoa implements Comparable<Pessoa>{
   
   int Id;
   String Nome;
   int Idade; 
   
   public Pessoa(){}
   
   public Pessoa(int Id, String Nome,int Idade) {
        this.Id = Id;
        this.Nome = Nome;
        this.Idade = Idade;
    }
   
    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public int getIdade() {
        return Idade;
    }

    public void setIdade(int Idade) {
        this.Idade = Idade;
    }
   


    @Override
    public int compareTo(Pessoa o) {
      
    	if(this.Idade < o.getIdade()){
    		return -1;
    	}
    	if(this.Idade > o.getIdade()){
    		return 1;
    	}
    	return 0;
    }

	public void print() {
		System.out.println("Id: "+ Id);
		System.out.println("Idade: "+Idade);
		System.out.println("Nome: "+Nome);
		
		
	}
	
}
