package questao2;

public class Banco {
	
	private static int indice;
	
	private Conta[] contas;
	
	private final int MAX_CONTAS = 5;
	public Banco () {
	 contas = new Conta[MAX_CONTAS];
	 indice = 0;
	}
	public void cadastrar(Conta c) {
	 if (indice < MAX_CONTAS) { 
	 contas[indice] = c;
	 indice = indice + 1;
	 System.out.println("Conta cadastrada com sucesso.");
	 } else {
	 System.out.println("Esta agencia est� cheia.Conta n�o cadastrada.");
	}
	}
	public void excluir(long numero){
	 Conta c;
	 c = procurar(numero);
	 if (c == null)
	 System.out.println("A conta " + numero + " nao existe!");
	 else{
	int i = 0;
	 for (i = 0; i < indice; i++){
	 if (contas[i].getNumero() == numero){
	
	 contas[i] = new Conta(0, 0);
	 System.out.println("Conta exclu�da com sucesso.");
	}
	}
	}
	}
	private Conta procurar(long numero) {
		 int i = 0; 
		 boolean achou = false;
		 Conta retorno;
		 while ((!achou) & (i < indice)) {
		 if (contas[i].getNumero() == numero)
		 achou = true;
		 else
		i = i + 1;
		}
		 if (achou == true)
		 retorno = contas[i];
		 else
		 retorno = null;
		 return retorno; 
		}
		public void printSaldo(long numero) {
		 Conta c;
		 c = procurar(numero);
		 if (c == null)
		 System.out.println("A conta " + numero + " nao existe!");
		 else
		 System.out.println("O saldo da conta " + numero +
		" �: " + c.getSaldo());
		}
		
		public void debitar(long numero, double valor) {
		 Conta c;
		 c = this.procurar(numero);
		 if (c == null)
		 System.out.println("A conta " + numero + " nao existe!");
		 else{
		 c.debitar(valor);
		 System.out.println("D�bito realizado com sucesso.");
		}
		}
		public void creditar(long numero, double valor) {
		 Conta c;
		 c = this.procurar(numero);
		 if (c == null)
		 System.out.println("A conta " + numero + " nao existe!");
		 else{
		 c.creditar(valor);
		 System.out.println("Cr�dito realizado com sucesso.");
		}
		}
		 public void transferir(long numeroDe, long numeroPara, double valor) {
			 Conta contaDe, contaPara;
			 contaDe = this.procurar(numeroDe);
			 contaPara = this.procurar(numeroPara);
			 if ((contaDe == null) || (contaPara == null))
			 System.out.println("Um das contas n�o existe!");
			 else {
			 contaDe.debitar(valor);
			 contaPara.creditar(valor);
			 System.out.println("Transfer�ncia realizada com sucesso.");
			}
			}
			 
		 public void getSaldoTotal() {
			 int i;
			 double total;
			total = 0.0;
			 for (i = 0; i < indice; i++)
			 total = total + contas[i].getSaldo();
			 System.out.println("O saldo total das contas �: " + total);
			}
		
			public int getNumeroClientes() {
			 return indice;
			}
			}
		