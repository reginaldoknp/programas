package questao2;
import java.util.Scanner;
public class Aplicativo {
	
	static Banco banco = new Banco();

	static Scanner tecla = new Scanner(System.in);
	public static void main(String[] args) {
	 int op;
	do {
	 System.out.println("::: MENU PRINCIPAL :::");
	 System.out.println("1-Cadastrar Conta");
	 System.out.println("2-Excluir Conta");
	 System.out.println("3-Saldo da Conta");
	 System.out.println("4-Debitar");
	 System.out.println("5-Creditar");
	 System.out.println("6-Transferir");
	 System.out.println("7-Imprimir o saldo das Contas");
	 System.out.println("8-Sair");
	 System.out.println("Digite sua op��o: ");
	 op = tecla.nextInt();
	 switch(op){
	 case 1: incluirConta(); break;
	 case 2: excluirConta(); break;
	 case 3: imprirSaldoDaConta(); break;
	 case 4: debitar(); break;
	 case 5: creditar(); break;
	 case 6: transferir(); break;
	 case 7: imprimirSaldoDasContas(); break;
	 case 8: break;
	}
	 } while (op!=8);
	}
	public static void incluirConta(){
	 
	 System.out.println("Digite o n�mero da conta:");
	 long num = tecla.nextLong();
	 System.out.println("Digite o saldo da conta:");
	 double saldo = tecla.nextDouble();
	
	 banco.cadastrar(new Conta(num, saldo));
	}
	public static void excluirConta(){
	 System.out.println("Digite o n�mero da conta:");
	 long num = tecla.nextLong();
	 banco.excluir(num);
	}
	public static void imprirSaldoDaConta(){
	
	 System.out.println("Digite o n�mero da conta:");
	 long num = tecla.nextLong();

	 banco.printSaldo(num);
	}
	public static void debitar(){
	
	 System.out.println("Digite o n�mero da conta:");
	 long num = tecla.nextLong();
	 System.out.println("Digite o valor do d�bito:");
	 double valor = tecla.nextDouble();
	
	 banco.debitar(num, valor);
	}
	public static void creditar(){
	
	 System.out.println("Digite o n�mero da conta:");
	 long num = tecla.nextLong();
	 System.out.println("Digite o valor do d�bito:");
	 double valor = tecla.nextDouble();
	
	 banco.creditar(num, valor);
	}
	public static void transferir(){
	 System.out.println("Digite o n�mero da conta (ORIGEM):");
	 long numOrigem = tecla.nextLong();
	 System.out.println("Digite o n�mero da conta (DESTINO):");
	 long numDestino = tecla.nextLong();
	 System.out.println("Digite o valor da transfer�ncia:");
	 double valor = tecla.nextDouble();

	 banco.transferir(numOrigem, numDestino, valor);
	}
	public static void imprimirSaldoDasContas(){
	 banco.getSaldoTotal();
	}
	}

