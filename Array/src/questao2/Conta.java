package questao2;

public class Conta {
	private long numero;
	private double saldo;
	public Conta(long n, double s){
	 numero = n;
	 saldo = s;
	}
	public long getNumero() {
	 return numero;
	}
	public void setNumero(long numero) {
	 this.numero = numero;
	}
	public double getSaldo() {
	 return saldo;
	}
	public void setSaldo(double saldo) {
	 this.saldo = saldo;
	}
	public void debitar(double soma){
	 saldo -= soma;
	}
	public void creditar(double soma){
		saldo += soma;
	}
}

	 
	